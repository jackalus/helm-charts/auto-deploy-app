# Getting Started

## Assumptions
By default, there are some assumptions done from GitLab that are not explained well. Instead of changing the default values, I will provide instructions to override. I want to keep the source code as close to the upstream as possible. The assumptions are are the following.
  
* The container port is exected to be `5000` even if you provide your own Dockerfile. 
    * Override via `HELM_EXTRA_ARGS` variable
        * `HELM_EXTRA_ARGS=--set service.internalPort=80 --set service.externalPort=80`

    * Override via `values.yaml`
        * `HELM_EXTRA_ARGS=-f values` 
        * `values.yaml`
          ```
          service:
              internalPort: 80
              externalPort: 80
          ```
* Postgres is enabled by default. I disabled it through my autodevops template.
    * Override via `POSTGRES_ENABLED` variable
        * `POSTGRES_ENABLED=false`


## Usage
There are two main use cases for auto-deploy-app:
1. A simple app that does not have dependencies.
2. An application that requires configurations/secrets.

### Simple Application

### Application with Vault secrets
Set `HELM_EXTRA_ARGS=-f values` in your CI/CD variables.

Define volumes.
```
volumes:
- name: secrets
  emptyDir: {}
- name: vault-tls
  secret:
    secretName: vault-tls
- name: vault-token
  emptyDir:
    medium: Memory
```

Add an initial container with the following:
* `vault` service account
* Vault address from the `vault` config map
* Vault CA cert from the `VAULT_CACERT` secret
* `demo` Vault Kubernetes auth role

We get back the following:
* User token stored in `/var/run/secrets/vaultproject.io`
  
```
initContainers:
- name: vault-authenticator
  image: sethvargo/vault-kubernetes-authenticator:0.2.0
  imagePullPolicy: IfNotPresent
  volumeMounts:
  - name: vault-token
    mountPath: /var/run/secrets/vaultproject.io
  - name: vault-tls
    mountPath: /etc/vault/tls
  env:
  - name: VAULT_ADDR
    valueFrom:
      configMapKeyRef:
        name: vault
        key: vault_addr
  - name: VAULT_CACERT
    value: /etc/vault/tls/ca.pem
  - name: VAULT_ROLE
    value: demo
serviceAccountName: vault
```

Create a sidecar that has the following:
* Access to the Vault user token
* Shares a mount with our application container
* Shares the process namespace with our application container
* consul-template configurations

```
additionalContainers:
  - name: consul-template
    image: hashicorp/consul-template:0.20.0-light
    imagePullPolicy: IfNotPresent
    securityContext:
      capabilities:
        add: ['SYS_PTRACE']
    volumeMounts:
    - name: secrets
      mountPath: /secrets
    - name: vault-tls
      mountPath: /etc/vault/tls
    - name: vault-token
      mountPath: /var/run/secrets/vaultproject.io
    env:
    - name: VAULT_ADDR
      valueFrom:
        configMapKeyRef:
          name: vault
          key: vault_addr
    - name: VAULT_CACERT
      value: /etc/vault/tls/ca.pem
    - name: CT_LOCAL_CONFIG
      value: |
        vault {
          vault_agent_token_file = "/var/run/secrets/vaultproject.io/.vault-token"
          ssl {
            ca_cert = "/etc/vault/tls/ca.pem"
          }
          retry {
            backoff = "1s"
          }
        }
        template {
          contents = <<EOH
        {{- with secret "database/creds/demo-role" }}
        MYSQL_URL=mysql://{{ .Data.username }}:{{ .Data.password }}@demo2db.stage.opcon.dev/mysql
        {{ end }}
        EOH
          destination = "/secrets/.env"
          command     = "/bin/sh -c \"kill -HUP $(pidof node index.js) || true\""
        }
```
Add in application container with the following:
* Command override with a symlink to the `.env` file incase you cannot override.
* Mount shared volume
* Any app specific configurations

```
application:
  command: [/bin/sh, "-c", "ln -s /secrets/.env /app/.env; node index.js"]
  volumeMounts:
  - name: secrets
    mountPath: /secrets/
service:
  internalPort: 8080
  externalPort: 8080
livenessProbe:
  path: version
readinessProbe:
  path: version
```
Your Helm `values.yaml` should look like this.
```
initContainers:
- name: vault-authenticator
  image: sethvargo/vault-kubernetes-authenticator:0.2.0
  imagePullPolicy: IfNotPresent
  volumeMounts:
  - name: vault-token
    mountPath: /var/run/secrets/vaultproject.io
  - name: vault-tls
    mountPath: /etc/vault/tls
  env:
  - name: VAULT_ADDR
    valueFrom:
      configMapKeyRef:
        name: vault
        key: vault_addr
  - name: VAULT_CACERT
    value: /etc/vault/tls/ca.pem
  - name: VAULT_ROLE
    value: demo
additionalContainers:
  - name: consul-template
    image: hashicorp/consul-template:0.20.0-light
    imagePullPolicy: IfNotPresent
    securityContext:
      capabilities:
        add: ['SYS_PTRACE']
    volumeMounts:
    - name: secrets
      mountPath: /secrets
    - name: vault-tls
      mountPath: /etc/vault/tls
    - name: vault-token
      mountPath: /var/run/secrets/vaultproject.io
    env:
    - name: VAULT_ADDR
      valueFrom:
        configMapKeyRef:
          name: vault
          key: vault_addr
    - name: VAULT_CACERT
      value: /etc/vault/tls/ca.pem
    - name: CT_LOCAL_CONFIG
      value: |
        vault {
          vault_agent_token_file = "/var/run/secrets/vaultproject.io/.vault-token"
          ssl {
            ca_cert = "/etc/vault/tls/ca.pem"
          }
          retry {
            backoff = "1s"
          }
        }
        template {
          contents = <<EOH
        {{- with secret "database/creds/demo-role" }}
        MYSQL_URL=mysql://{{ .Data.username }}:{{ .Data.password }}@demo2db.stage.opcon.dev/mysql
        {{ end }}
        EOH
          destination = "/secrets/.env"
          command     = "/bin/sh -c \"kill -HUP $(pidof node index.js) || true\""
        }
volumes:
- name: secrets
  emptyDir: {}
- name: vault-tls
  secret:
    secretName: vault-tls
- name: vault-token
  emptyDir:
    medium: Memory
application:
  command: [/bin/sh, "-c", "ln -s /secrets/.env /app/.env; node index.js"]
  volumeMounts:
  - name: secrets
    mountPath: /secrets/
service:
  internalPort: 8080
  externalPort: 8080
livenessProbe:
  path: version
readinessProbe:
  path: version
shareProcessNamespace: true
serviceAccountName: vault
```